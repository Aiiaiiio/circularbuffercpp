#include <iostream>
#include "../circularbuffer.h"

using namespace estd;

template <typename T>
void printBuffer( const CircularBuffer<T>& buffer, const char* name = "" )
{
    std::string name_str( name );
    std::cout << ( buffer.full() ? "* | " : "  | " );
    for( size_t i = 0u; i < buffer.size(); ++i )
    { std::cout << buffer[i] << ", "; }
    std::cout << " (" << buffer.size() << '/' << buffer.capacity() << ')';
    if( !name_str.empty() )
    { std::cout << " [" << name_str << ']'; }
    std::cout << std::endl;
}

void test_0()
{ // Push and pop 1
    puts( "\n\n---- TEST 0 -----" );
    CircularBuffer<char> cbuff( 5 );

    for( char c = 'A'; c < 'Z'; ++c )
    {
        cbuff.push_back( c );
        printBuffer( cbuff );
    }

    while( !cbuff.empty() )
    {
        cbuff.pop_front();
        printBuffer( cbuff );
    }
}

void test_1()
{ // Push and pop 2
    puts( "\n\n---- TEST 1 -----" );
    CircularBuffer<char> cbuff( 5 );

    puts( "  Push 3" );
    for( int i = 0; i < 3; ++i )
    {
        cbuff.push_back( 'A' + i );
        printBuffer( cbuff );
    }

    puts( "  Pop 2" );
    for( int i = 0; i < 2; ++i )
    {
        cbuff.pop_front();
        printBuffer( cbuff );
    }

    puts( "  Push 5" );
    for( int i = 0; i < 5; ++i )
    {
        cbuff.push_back( 'D' + i );
        printBuffer( cbuff );
    }

    puts( "  Pop 2" );
    for( int i = 0; i < 2; ++i )
    {
        cbuff.pop_front();
        printBuffer( cbuff );
    }

    puts( "  Push 10" );
    for( int i = 0; i < 10; ++i )
    {
        cbuff.push_back( 'I' + i );
        printBuffer( cbuff );
    }
}

void test_2()
{ // Push and pop 2
    puts( "\n\n---- TEST 2 -----" );
    CircularBuffer<char> cbuff( 5 );

    puts( "  Push 3" );
    for( int i = 0; i < 3; ++i )
    {
        cbuff.push_back( 'A' + i );
        printBuffer( cbuff );
    }

    puts( "  Pop 2" );
    for( int i = 0; i < 2; ++i )
    {
        cbuff.pop_front();
        printBuffer( cbuff );
    }

    puts( "  Resize to 8" );
    cbuff.set_capacity( 8 );
    printBuffer( cbuff );

    puts( "  Push 5" );
    for( int i = 0; i < 5; ++i )
    {
        cbuff.push_back( 'D' + i );
        printBuffer( cbuff );
    }

    puts( "  Pop 2" );
    for( int i = 0; i < 2; ++i )
    {
        cbuff.pop_front();
        printBuffer( cbuff );
    }

    puts( "  Push 10" );
    for( int i = 0; i < 10; ++i )
    {
        cbuff.push_back( 'I' + i );
        printBuffer( cbuff );
    }

    puts( "  Resize to 2" );
    cbuff.set_capacity( 2 );
    printBuffer( cbuff );

    puts( "  Push 3" );
    for( int i = 0; i < 3; ++i )
    {
        cbuff.push_back( '0' + i );
        printBuffer( cbuff );
    }
}

void test_3()
{ // Clear and copy/move
    puts( "\n\n---- TEST 3 -----" );
    CircularBuffer<char> cbuff( 5 );

    puts( "  Push 10" );
    for( char c = 'A'; c < ( 'A'+ 10 ); ++c )
    {
        cbuff.push_back( c );
    }
    printBuffer( cbuff, "cbuff" );

    {
        puts( "  Copy construct" );
        CircularBuffer<char> cbuff2( cbuff );
        printBuffer( cbuff, "cbuff" );
        printBuffer( cbuff2, "cbuff2" );
    }

    {
        puts( "  Move construct" );
        CircularBuffer<char> cbuff2( std::move( cbuff ) );
        printBuffer( cbuff, "cbuff" );
        printBuffer( cbuff2, "cbuff2" );
    }
}

void test_4()
{ // Iterator
    puts( "\n\n---- TEST 4 -----" );

    CircularBuffer<char> cbuff( 5 );

    for( char c = 'A'; c < 'Z'; ++c )
    {
        cbuff.push_back( c );
        printBuffer( cbuff );
    }

    std::cout << "Walk by iterator: (" << cbuff.size() << ')' << std::endl;
    for( auto& i : cbuff )
    { std::cout << i << ", "; }
    std::cout << std::endl;
}

void test_5()
{ // Const iterator
    puts( "\n\n---- TEST 5 -----" );

    CircularBuffer<char> cbuff( 5 );

    for( char c = 'A'; c < 'Z'; ++c )
    {
        cbuff.push_back( c );
        printBuffer( cbuff );
    }

    {
        const CircularBuffer<char>& ccbuff = cbuff;
        std::cout << "Const walk by const iterator: (" << ccbuff.size() << ')' << std::endl;
        for( auto& i : ccbuff )
        { std::cout << i << ", "; }
        std::cout << std::endl;
    }
}

void test_6()
{ // Reverse iterator
    puts( "\n\n---- TEST 6 -----" );

    CircularBuffer<char> cbuff( 5 );

    for( char c = 'A'; c < 'Z'; ++c )
    {
        cbuff.push_back( c );
        printBuffer( cbuff );
    }

    std::cout << "Walk by reverse iterator: (" << cbuff.size() << ')' << std::endl;
    for( auto i = cbuff.rbegin(), end = cbuff.rend(); i != end; ++i )
    { std::cout << *i << ", "; }
    std::cout << std::endl;
}

void test_7()
{ // Const reverse iterator
    puts( "\n\n---- TEST 7 -----" );

    CircularBuffer<char> cbuff( 5 );

    for( char c = 'A'; c < 'Z'; ++c )
    {
        cbuff.push_back( c );
        printBuffer( cbuff );
    }

    {
        const CircularBuffer<char>& ccbuff = cbuff;
        std::cout << "Const walk by const reverse iterator: (" << ccbuff.size() << ')' << std::endl;
        for( auto i = ccbuff.crbegin(), end = ccbuff.crend(); i != end; ++i )
        { std::cout << *i << ", "; }
        std::cout << std::endl;
    }
}

int main()
{
    test_0();
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    test_6();
    test_7();
    return 0;
}
