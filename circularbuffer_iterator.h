#ifndef _CIRCULAR_BUFFER_ITERATOR_
#define _CIRCULAR_BUFFER_ITERATOR_

#include "circularbuffer.h"

namespace estd
{
    template <typename T>
    class CircularBufferIteratorBase
    {
        public:
            bool isValid() const;
            bool operator==( const CircularBufferIteratorBase& rhs ) const;
            bool operator!=( const CircularBufferIteratorBase& rhs ) const;

        protected:
            CircularBufferIteratorBase();
            CircularBufferIteratorBase( const CircularBufferIteratorBase& src );
            CircularBufferIteratorBase( CircularBufferIteratorBase&& src );
            CircularBufferIteratorBase( const CircularBuffer<T>& container, size_t index );

            void forward();
            void backward();
            void forward( size_t steps );
            void backward( size_t steps );

            void assign( const CircularBufferIteratorBase& src );
            void assign( CircularBufferIteratorBase&& src );

            const T& operator*() const;
            const T* operator->() const;

            CircularBuffer<T> const*    mContainer;
            size_t                      mCurrentIndex;
    };

    template <typename T> class CircularBufferIterator;
    template <typename T> class CircularBufferReverseIterator;
    template <typename T> class CircularBufferConstIterator;
    template <typename T> class CircularBufferConstReverseIterator;

    template <typename T>
    class CircularBufferIterator : public CircularBufferIteratorBase<T>
    {
        friend class CircularBuffer<T>;

        public:
            CircularBufferIterator();
            CircularBufferIterator( const CircularBufferIterator& src );
            CircularBufferIterator( CircularBufferIterator&& src );

            CircularBufferIterator& operator=( const CircularBufferIterator<T>& src );
            CircularBufferIterator& operator=( CircularBufferIterator<T>&& src );
            CircularBufferIterator& operator=( const CircularBufferReverseIterator<T>& src );
            CircularBufferIterator& operator=( CircularBufferReverseIterator<T>&& src );

            T& operator*();
            T* operator->();

            
            CircularBufferIterator operator++( int );
            CircularBufferIterator& operator++();
            CircularBufferIterator operator--( int );
            CircularBufferIterator& operator--();
            CircularBufferIterator& operator+=( size_t i );
            CircularBufferIterator& operator-=( size_t i );
            CircularBufferIterator operator+( size_t i );
            CircularBufferIterator operator-( size_t i );

        private:
            CircularBufferIterator( CircularBuffer<T>& container, size_t index );
    };

    template <typename T>
    class CircularBufferReverseIterator : public CircularBufferIteratorBase<T>
    {
        friend class CircularBuffer<T>;

        public:
            CircularBufferReverseIterator();
            CircularBufferReverseIterator( const CircularBufferReverseIterator& src );
            CircularBufferReverseIterator( CircularBufferReverseIterator&& src );

            CircularBufferReverseIterator& operator=( const CircularBufferIterator<T>& src );
            CircularBufferReverseIterator& operator=( CircularBufferIterator<T>&& src );
            CircularBufferReverseIterator& operator=( const CircularBufferReverseIterator<T>& src );
            CircularBufferReverseIterator& operator=( CircularBufferReverseIterator<T>&& src );

            T& operator*();
            T* operator->();


            CircularBufferReverseIterator operator++( int );
            CircularBufferReverseIterator& operator++();
            CircularBufferReverseIterator operator--( int );
            CircularBufferReverseIterator& operator--();
            CircularBufferReverseIterator& operator+=( size_t i );
            CircularBufferReverseIterator& operator-=( size_t i );
            CircularBufferReverseIterator operator+( size_t i );
            CircularBufferReverseIterator operator-( size_t i );

        private:
            CircularBufferReverseIterator( CircularBuffer<T>& container, size_t index );
    };

    template <typename T>
    class CircularBufferConstIterator : public CircularBufferIteratorBase<T>
    {
        friend class CircularBuffer<T>;

        public:
            CircularBufferConstIterator();
            CircularBufferConstIterator( const CircularBufferConstIterator& src );
            CircularBufferConstIterator( CircularBufferConstIterator&& src );

            CircularBufferConstIterator& operator=( const CircularBufferIteratorBase<T>& src );
            CircularBufferConstIterator& operator=( CircularBufferIteratorBase<T>&& src );

            const T& operator*() const;
            const T* operator->() const;


            CircularBufferConstIterator operator++( int );
            CircularBufferConstIterator& operator++();
            CircularBufferConstIterator operator--( int );
            CircularBufferConstIterator& operator--();
            CircularBufferConstIterator& operator+=( size_t i );
            CircularBufferConstIterator& operator-=( size_t i );
            CircularBufferConstIterator operator+( size_t i );
            CircularBufferConstIterator operator-( size_t i );

        private:
            CircularBufferConstIterator( const CircularBuffer<T>& container, size_t index );
    };

    template <typename T>
    class CircularBufferConstReverseIterator : public CircularBufferIteratorBase<T>
    {
        friend class CircularBuffer<T>;

        public:
            CircularBufferConstReverseIterator();
            CircularBufferConstReverseIterator( const CircularBufferConstReverseIterator& src );
            CircularBufferConstReverseIterator( CircularBufferConstReverseIterator&& src );

            CircularBufferConstReverseIterator& operator=( const CircularBufferIteratorBase<T>& src );
            CircularBufferConstReverseIterator& operator=( CircularBufferIteratorBase<T>&& src );

            const T& operator*() const;
            const T* operator->() const;


            CircularBufferConstReverseIterator operator++( int );
            CircularBufferConstReverseIterator& operator++();
            CircularBufferConstReverseIterator operator--( int );
            CircularBufferConstReverseIterator& operator--();
            CircularBufferConstReverseIterator& operator+=( size_t i );
            CircularBufferConstReverseIterator& operator-=( size_t i );
            CircularBufferConstReverseIterator operator+( size_t i );
            CircularBufferConstReverseIterator operator-( size_t i );

        private:
            CircularBufferConstReverseIterator( const CircularBuffer<T>& container, size_t index );
    };




    //////////////////////////////////////////////
    ////// Base

    template <typename T>
    bool CircularBufferIteratorBase<T>::isValid() const
    { return mContainer && mCurrentIndex < mContainer->size(); }

    template <typename T>
    bool CircularBufferIteratorBase<T>::operator==( const CircularBufferIteratorBase& rhs ) const
    { return mContainer == rhs.mContainer && mCurrentIndex == rhs.mCurrentIndex; }

    template <typename T>
    bool CircularBufferIteratorBase<T>::operator!=( const CircularBufferIteratorBase& rhs ) const
    { return mContainer != rhs.mContainer || mCurrentIndex != rhs.mCurrentIndex; }

    template <typename T>
    CircularBufferIteratorBase<T>::CircularBufferIteratorBase()
    :   mContainer( nullptr )
    ,   mCurrentIndex( 0u )
    {}

    template <typename T>
    CircularBufferIteratorBase<T>::CircularBufferIteratorBase( const CircularBufferIteratorBase& src )
    :   mContainer( src.mContainer )
    ,   mCurrentIndex( src.mCurrentIndex )
    {}

    template <typename T>
    CircularBufferIteratorBase<T>::CircularBufferIteratorBase( CircularBufferIteratorBase&& src )
    :   mContainer( std::move( src.mContainer ) ) // TODO review!
    ,   mCurrentIndex( std::move( src.mCurrentIndex ) )
    {}

    template <typename T>
    CircularBufferIteratorBase<T>::CircularBufferIteratorBase( const CircularBuffer<T>& container, size_t index )
    :   mContainer( &container )
    ,   mCurrentIndex( index )
    {}

    template <typename T>
    void CircularBufferIteratorBase<T>::forward()
    {
        if( isValid() )
        { ++mCurrentIndex; }
    }

    template <typename T>
    void CircularBufferIteratorBase<T>::backward()
    {
        if( mCurrentIndex <= 0u )
        { mCurrentIndex = mContainer->size(); }
        else
        { --mCurrentIndex; }
    }

    template <typename T>
    void CircularBufferIteratorBase<T>::forward( size_t steps )
    {
        if( isValid() )
        { mCurrentIndex += std::min( mContainer->size() - mCurrentIndex, steps ); }
    }

    template <typename T>
    void CircularBufferIteratorBase<T>::backward( size_t steps )
    {
        if( steps > mCurrentIndex )
        { mCurrentIndex = mContainer->size(); }
        else
        { mCurrentIndex -= steps; }
    }

    template <typename T>
    void CircularBufferIteratorBase<T>::assign( const CircularBufferIteratorBase& src )
    {
        mContainer = src.mContainer;
        mCurrentIndex = src.mCurrentIndex;
    }

    template <typename T>
    void CircularBufferIteratorBase<T>::assign( CircularBufferIteratorBase&& src )
    {
        std::swap( mContainer, src.mContainer );
        std::swap( mCurrentIndex, src.mCurrentIndex );
    }

    template <typename T>
    const T& CircularBufferIteratorBase<T>::operator*() const
    { return mContainer->at( mCurrentIndex ); }

    template <typename T>
    const T* CircularBufferIteratorBase<T>::operator->() const
    { return &mContainer->at( mCurrentIndex ); }




    //////////////////////////////////////////////
    ////// Iterator

    template <typename T>
    CircularBufferIterator<T>::CircularBufferIterator()
    :   CircularBufferIteratorBase<T>()
    {}

    template <typename T>
    CircularBufferIterator<T>::CircularBufferIterator( const CircularBufferIterator<T>& src )
    :   CircularBufferIteratorBase<T>( src )
    {}

    template <typename T>
    CircularBufferIterator<T>::CircularBufferIterator( CircularBufferIterator<T>&& src )
    :   CircularBufferIteratorBase<T>( std::forward( src ) )
    {}

    template <typename T>
    CircularBufferIterator<T>& CircularBufferIterator<T>::operator=( const CircularBufferIterator<T>& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    CircularBufferIterator<T>& CircularBufferIterator<T>::operator=( CircularBufferIterator<T>&& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    CircularBufferIterator<T>& CircularBufferIterator<T>::operator=( const CircularBufferReverseIterator<T>& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    CircularBufferIterator<T>& CircularBufferIterator<T>::operator=( CircularBufferReverseIterator<T>&& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    T& CircularBufferIterator<T>::operator*()
    { return const_cast<T&>( CircularBufferIteratorBase<T>::operator*() ); }

    template <typename T>
    T* CircularBufferIterator<T>::operator->()
    { return const_cast<T*>( CircularBufferIteratorBase<T>::operator->() ); }

    template <typename T>
    CircularBufferIterator<T> CircularBufferIterator<T>::operator++( int )
    {
        CircularBufferIterator<T> current_state( *this );
        CircularBufferIteratorBase<T>::forward();
        return current_state;
    }

    template <typename T>
    CircularBufferIterator<T>& CircularBufferIterator<T>::operator++()
    {
        CircularBufferIteratorBase<T>::forward();
        return *this;
    }

    template <typename T>
    CircularBufferIterator<T> CircularBufferIterator<T>::operator--( int )
    {
        CircularBufferIterator<T> current_state( *this );
        CircularBufferIteratorBase<T>::backward();
        return current_state;
    }

    template <typename T>
    CircularBufferIterator<T>& CircularBufferIterator<T>::operator--()
    {
        CircularBufferIteratorBase<T>::backward();
        return *this;
    }

    template <typename T>
    CircularBufferIterator<T>& CircularBufferIterator<T>::operator+=( size_t i )
    {
        CircularBufferIteratorBase<T>::forward( i );
        return *this;
    }

    template <typename T>
    CircularBufferIterator<T>& CircularBufferIterator<T>::operator-=( size_t i )
    {
        CircularBufferIteratorBase<T>::backward( i );
        return *this;
    }

    template <typename T>
    CircularBufferIterator<T> CircularBufferIterator<T>::operator+( size_t i )
    {
        CircularBufferIterator<T> current_state( *this );
        current_state += i;
        return current_state;
    }

    template <typename T>
    CircularBufferIterator<T> CircularBufferIterator<T>::operator-( size_t i )
    {
        CircularBufferIterator<T> current_state( *this );
        current_state -= i;
        return current_state;
    }

    template <typename T>
    CircularBufferIterator<T>::CircularBufferIterator( CircularBuffer<T>& container, size_t index )
    :   CircularBufferIteratorBase<T>( container, index )
    {}




    //////////////////////////////////////////////
    ////// Reverse iterator

    template <typename T>
    CircularBufferReverseIterator<T>::CircularBufferReverseIterator()
    :   CircularBufferIteratorBase<T>()
    {}

    template <typename T>
    CircularBufferReverseIterator<T>::CircularBufferReverseIterator( const CircularBufferReverseIterator<T>& src )
    :   CircularBufferIteratorBase<T>( src )
    {}

    template <typename T>
    CircularBufferReverseIterator<T>::CircularBufferReverseIterator( CircularBufferReverseIterator<T>&& src )
    :   CircularBufferIteratorBase<T>( std::forward( src ) )
    {}

    template <typename T>
    CircularBufferReverseIterator<T>& CircularBufferReverseIterator<T>::operator=( const CircularBufferIterator<T>& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    CircularBufferReverseIterator<T>& CircularBufferReverseIterator<T>::operator=( CircularBufferIterator<T>&& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    CircularBufferReverseIterator<T>& CircularBufferReverseIterator<T>::operator=( const CircularBufferReverseIterator<T>& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    CircularBufferReverseIterator<T>& CircularBufferReverseIterator<T>::operator=( CircularBufferReverseIterator<T>&& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    T& CircularBufferReverseIterator<T>::operator*()
    { return const_cast<T&>( CircularBufferIteratorBase<T>::operator*() ); }

    template <typename T>
    T* CircularBufferReverseIterator<T>::operator->()
    { return const_cast<T*>( CircularBufferIteratorBase<T>::operator->() ); }

    template <typename T>
    CircularBufferReverseIterator<T> CircularBufferReverseIterator<T>::operator++( int )
    {
        CircularBufferReverseIterator<T> current_state( *this );
        CircularBufferIteratorBase<T>::backward();
        return current_state;
    }

    template <typename T>
    CircularBufferReverseIterator<T>& CircularBufferReverseIterator<T>::operator++()
    {
        CircularBufferIteratorBase<T>::backward();
        return *this;
    }

    template <typename T>
    CircularBufferReverseIterator<T> CircularBufferReverseIterator<T>::operator--( int )
    {
        CircularBufferReverseIterator<T> current_state( *this );
        CircularBufferIteratorBase<T>::forward();
        return current_state;
    }

    template <typename T>
    CircularBufferReverseIterator<T>& CircularBufferReverseIterator<T>::operator--()
    {
        CircularBufferIteratorBase<T>::forward();
        return *this;
    }

    template <typename T>
    CircularBufferReverseIterator<T>& CircularBufferReverseIterator<T>::operator+=( size_t i )
    {
        CircularBufferIteratorBase<T>::backward( i );
        return *this;
    }

    template <typename T>
    CircularBufferReverseIterator<T>& CircularBufferReverseIterator<T>::operator-=( size_t i )
    {
        CircularBufferIteratorBase<T>::forward( i );
        return *this;
    }

    template <typename T>
    CircularBufferReverseIterator<T> CircularBufferReverseIterator<T>::operator+( size_t i )
    {
        CircularBufferReverseIterator<T> current_state( *this );
        current_state -= i;
        return current_state;
    }

    template <typename T>
    CircularBufferReverseIterator<T> CircularBufferReverseIterator<T>::operator-( size_t i )
    {
        CircularBufferReverseIterator<T> current_state( *this );
        current_state += i;
        return current_state;
    }

    template <typename T>
    CircularBufferReverseIterator<T>::CircularBufferReverseIterator( CircularBuffer<T>& container, size_t index )
    :   CircularBufferIteratorBase<T>( container, index )
    {}




    //////////////////////////////////////////////
    ////// Const iterator

    template <typename T>
    CircularBufferConstIterator<T>::CircularBufferConstIterator()
    :   CircularBufferIteratorBase<T>()
    {}

    template <typename T>
    CircularBufferConstIterator<T>::CircularBufferConstIterator( const CircularBufferConstIterator& src )
    :   CircularBufferIteratorBase<T>( src )
    {}

    template <typename T>
    CircularBufferConstIterator<T>::CircularBufferConstIterator( CircularBufferConstIterator&& src )
    :   CircularBufferIteratorBase<T>( std::forward( src ) )
    {}

    template <typename T>
    CircularBufferConstIterator<T>& CircularBufferConstIterator<T>::operator=( const CircularBufferIteratorBase<T>& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    CircularBufferConstIterator<T>& CircularBufferConstIterator<T>::operator=( CircularBufferIteratorBase<T>&& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    const T& CircularBufferConstIterator<T>::operator*() const
    { return const_cast<T&>( CircularBufferIteratorBase<T>::operator*() ); }

    template <typename T>
    const T* CircularBufferConstIterator<T>::operator->() const
    { return const_cast<T*>( CircularBufferIteratorBase<T>::operator->() ); }

    template <typename T>
    CircularBufferConstIterator<T> CircularBufferConstIterator<T>::operator++( int )
    {
        CircularBufferConstIterator<T> current_state( *this );
        CircularBufferIteratorBase<T>::forward();
        return current_state;
    }

    template <typename T>
    CircularBufferConstIterator<T>& CircularBufferConstIterator<T>::operator++()
    {
        CircularBufferIteratorBase<T>::forward();
        return *this;
    }

    template <typename T>
    CircularBufferConstIterator<T> CircularBufferConstIterator<T>::operator--( int )
    {
        CircularBufferConstIterator<T> current_state( *this );
        CircularBufferIteratorBase<T>::backward();
        return current_state;
    }

    template <typename T>
    CircularBufferConstIterator<T>& CircularBufferConstIterator<T>::operator--()
    {
        CircularBufferIteratorBase<T>::backward();
        return *this;
    }

    template <typename T>
    CircularBufferConstIterator<T>& CircularBufferConstIterator<T>::operator+=( size_t i )
    {
        CircularBufferIteratorBase<T>::forward( i );
        return *this;
    }

    template <typename T>
    CircularBufferConstIterator<T>& CircularBufferConstIterator<T>::operator-=( size_t i )
    {
        CircularBufferIteratorBase<T>::backward( i );
        return *this;
    }

    template <typename T>
    CircularBufferConstIterator<T> CircularBufferConstIterator<T>::operator+( size_t i )
    {
        CircularBufferConstIterator<T> current_state( *this );
        current_state += i;
        return current_state;
    }

    template <typename T>
    CircularBufferConstIterator<T> CircularBufferConstIterator<T>::operator-( size_t i )
    {
        CircularBufferConstIterator<T> current_state( *this );
        current_state -= i;
        return current_state;
    }

    template <typename T>
    CircularBufferConstIterator<T>::CircularBufferConstIterator( const CircularBuffer<T>& container, size_t index )
    :   CircularBufferIteratorBase<T>( container, index )
    {}




    //////////////////////////////////////////////
    ////// Const reverse iterator

    template <typename T>
    CircularBufferConstReverseIterator<T>::CircularBufferConstReverseIterator()
    :   CircularBufferIteratorBase<T>()
    {}

    template <typename T>
    CircularBufferConstReverseIterator<T>::CircularBufferConstReverseIterator( const CircularBufferConstReverseIterator& src )
    :   CircularBufferIteratorBase<T>( src )
    {}

    template <typename T>
    CircularBufferConstReverseIterator<T>::CircularBufferConstReverseIterator( CircularBufferConstReverseIterator&& src )
    :   CircularBufferIteratorBase<T>( std::forward( src ) )
    {}

    template <typename T>
    CircularBufferConstReverseIterator<T>& CircularBufferConstReverseIterator<T>::operator=( const CircularBufferIteratorBase<T>& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T>& CircularBufferConstReverseIterator<T>::operator=( CircularBufferIteratorBase<T>&& src )
    {
        CircularBufferIteratorBase<T>::assign( src );
        return *this;
    }

    template <typename T>
    const T& CircularBufferConstReverseIterator<T>::operator*() const
    { return const_cast<T&>( CircularBufferIteratorBase<T>::operator*() ); }

    template <typename T>
    const T* CircularBufferConstReverseIterator<T>::operator->() const
    { return const_cast<T*>( CircularBufferIteratorBase<T>::operator->() ); }

    template <typename T>
    CircularBufferConstReverseIterator<T> CircularBufferConstReverseIterator<T>::operator++( int )
    {
        CircularBufferConstReverseIterator<T> current_state( *this );
        CircularBufferIteratorBase<T>::backward();
        return current_state;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T>& CircularBufferConstReverseIterator<T>::operator++()
    {
        CircularBufferIteratorBase<T>::backward();
        return *this;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T> CircularBufferConstReverseIterator<T>::operator--( int )
    {
        CircularBufferConstReverseIterator<T> current_state( *this );
        CircularBufferIteratorBase<T>::forward();
        return current_state;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T>& CircularBufferConstReverseIterator<T>::operator--()
    {
        CircularBufferIteratorBase<T>::forward();
        return *this;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T>& CircularBufferConstReverseIterator<T>::operator+=( size_t i )
    {
        CircularBufferIteratorBase<T>::backward( i );
        return *this;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T>& CircularBufferConstReverseIterator<T>::operator-=( size_t i )
    {
        CircularBufferIteratorBase<T>::forward( i );
        return *this;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T> CircularBufferConstReverseIterator<T>::operator+( size_t i )
    {
        CircularBufferConstReverseIterator<T> current_state( *this );
        current_state -= i;
        return current_state;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T> CircularBufferConstReverseIterator<T>::operator-( size_t i )
    {
        CircularBufferConstReverseIterator<T> current_state( *this );
        current_state += i;
        return current_state;
    }

    template <typename T>
    CircularBufferConstReverseIterator<T>::CircularBufferConstReverseIterator( const CircularBuffer<T>& container, size_t index )
    :   CircularBufferIteratorBase<T>( container, index )
    {}
}

#endif /* _CIRCULAR_BUFFER_ITERATOR_ */
