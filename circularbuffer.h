#ifndef _CIRCULARBUFFER_H_
#define _CIRCULARBUFFER_H_

#include <memory>

namespace estd
{
    template <typename T>
    class CircularBuffer;
}

#include "circularbuffer_iterator.h"

namespace estd
{
    /**
     * Circular buffer template
     */
    template <typename T>
    class CircularBuffer
    {
        public:
            CircularBuffer( size_t capacity = DEFAULT_CIRCULAR_BUFFER_SIZE );
            ~CircularBuffer();

            CircularBuffer( const CircularBuffer& src );
            CircularBuffer( CircularBuffer&& src );

            CircularBuffer& operator=( const CircularBuffer& rhs );
            CircularBuffer& operator=( CircularBuffer&& rhs );

            size_t size() const;
            size_t capacity() const;
            bool empty() const;
            bool full() const;

            void set_capacity( size_t capacity );
            void clear();

            void push_back( const T& val );
            void pop_front();

            const T& front() const;
            T& front();
            const T& back() const;
            T& back();

            const T& at( size_t index ) const;
            T& at( size_t index );

            const T& operator[]( size_t index ) const;
            T& operator[]( size_t index );

            CircularBufferIterator<T> begin();
            CircularBufferIterator<T> end();
            CircularBufferConstIterator<T> begin() const;
            CircularBufferConstIterator<T> end() const;
            CircularBufferConstIterator<T> cbegin() const;
            CircularBufferConstIterator<T> cend() const;
            CircularBufferReverseIterator<T> rbegin();
            CircularBufferReverseIterator<T> rend();
            CircularBufferConstReverseIterator<T> rbegin() const;
            CircularBufferConstReverseIterator<T> rend() const;
            CircularBufferConstReverseIterator<T> crbegin() const;
            CircularBufferConstReverseIterator<T> crend() const;

        private:
            static constexpr size_t DEFAULT_CIRCULAR_BUFFER_SIZE = 128u;
            void reserve( size_t new_capacity );

            T*                      mBuffer;
            size_t                  mCapacity;
            size_t                  mBeginIndex;
            size_t                  mSize;
    };

    template <typename T>
    CircularBuffer<T>::CircularBuffer( size_t capacity )
    :   mBuffer( reinterpret_cast<T*>( calloc( capacity, sizeof( T ) ) ) )
    ,   mCapacity( capacity )
    ,   mBeginIndex( 0u )
    ,   mSize( 0u )
    {}

    template <typename T>
    CircularBuffer<T>::~CircularBuffer()
    {
        clear();
        free( mBuffer );
    }

    template <typename T>
    CircularBuffer<T>::CircularBuffer( const CircularBuffer& src )
    :   mBuffer( nullptr )
    ,   mCapacity( 0u )
    ,   mBeginIndex( 0u )
    ,   mSize( 0u )
    {
        reserve( src.mCapacity );
        for( size_t i = 0u; i < mSize; ++i )
        { mBuffer[i] = src.at( i ); }

        mCapacity = src.mCapacity;
        mBeginIndex = src.mBeginIndex;
        mSize = src.mSize;
    }

    template <typename T>
    CircularBuffer<T>::CircularBuffer( CircularBuffer&& src )
    :   mBuffer( nullptr )
    ,   mCapacity( 0u )
    ,   mBeginIndex( 0u )
    ,   mSize( 0u )
    {
        std::swap( mBeginIndex, src.mBeginIndex );
        std::swap( mSize, src.mSize );
        std::swap( mCapacity, src.mCapacity );
        std::swap( mBuffer, src.mBuffer );
    }

    template <typename T>
    CircularBuffer<T>& CircularBuffer<T>::operator=( const CircularBuffer& rhs )
    {
        reserve( rhs.mCapacity );
        for( size_t i = 0u; i < mSize; ++i )
        { mBuffer[i] = rhs.at( i ); }

        mCapacity = rhs.mCapacity;
        mBeginIndex = rhs.mBeginIndex;
        mSize = rhs.mSize;
        return *this;
    }

    template <typename T>
    CircularBuffer<T>& CircularBuffer<T>::operator=( CircularBuffer&& rhs )
    {
        std::swap( mBeginIndex, rhs.mBeginIndex );
        std::swap( mSize, rhs.mSize );
        std::swap( mCapacity, rhs.mCapacity );
        std::swap( mBuffer, rhs.mBuffer );
        return *this;
    }

    template <typename T>
    size_t CircularBuffer<T>::size() const
    { return mSize; }

    template <typename T>
    size_t CircularBuffer<T>::capacity() const
    { return mCapacity; }

    template <typename T>
    bool CircularBuffer<T>::empty() const
    { return mSize <= 0; }

    template <typename T>
    bool CircularBuffer<T>::full() const
    { return mSize >= mCapacity; }

    template <typename T>
    void CircularBuffer<T>::set_capacity( size_t capacity )
    {
        if( capacity != mCapacity )
        {
            // reserve new buffer
            auto new_buffer = reinterpret_cast<decltype( mBuffer )>( calloc( capacity, sizeof( T ) ) );

            // copy old items
            for( size_t i = 0u, end = std::min( mSize, capacity ); i < end; ++i )
            { new_buffer[i] = at( i ); }

            // destruct items that would not fit any more
            for( size_t i = capacity; i < mSize; ++i )
            { at( i ).~T(); }

            // release old buffer
            free( mBuffer );

            // set members
            mBuffer = new_buffer;
            mBeginIndex = 0u;
            mSize = std::min( mSize, capacity );
            mCapacity = capacity;


        }
    }

    template <typename T>
    void CircularBuffer<T>::clear()
    {
        for( size_t i = 0u; i < mSize; ++i )
        { mBuffer[ ( mBeginIndex + i ) % mCapacity ].~T(); }
        mBeginIndex = 0u;
        mSize = 0u;
    }

    template <typename T>
    void CircularBuffer<T>::push_back( const T& val )
    {
        mBuffer[ ( mBeginIndex + mSize ) % mCapacity ] = val;
        if( mSize < mCapacity )
        { ++mSize; }
        else
        { mBeginIndex = ( mBeginIndex + 1 ) % mCapacity; }
    }

    template <typename T>
    void CircularBuffer<T>::pop_front()
    {
        mBuffer[ mBeginIndex ].~T();
        mBeginIndex = ( mBeginIndex + 1 ) % mCapacity;
        --mSize;
    }

    template <typename T>
    const T& CircularBuffer<T>::front() const
    { return mBuffer[ mBeginIndex ]; }

    template <typename T>
    T& CircularBuffer<T>::front()
    { return mBuffer[ mBeginIndex ]; }

    template <typename T>
    const T& CircularBuffer<T>::back() const
    { return mBuffer[ ( mBeginIndex + mSize - 1 ) % mCapacity ]; }

    template <typename T>
    T& CircularBuffer<T>::back()
    { return mBuffer[ ( mBeginIndex + mSize - 1 ) % mCapacity ]; }

    template <typename T>
    const T& CircularBuffer<T>::at( size_t index ) const
    { return mBuffer[ ( mBeginIndex + index ) % mCapacity ]; }

    template <typename T>
    T& CircularBuffer<T>::at( size_t index )
    { return mBuffer[ ( mBeginIndex + index ) % mCapacity ]; }

    template <typename T>
    const T& CircularBuffer<T>::operator[]( size_t index ) const
    { return mBuffer[ ( mBeginIndex + index ) % mCapacity ]; }

    template <typename T>
    T& CircularBuffer<T>::operator[]( size_t index )
    { return mBuffer[ ( mBeginIndex + index ) % mCapacity ]; }

    template <typename T>
    CircularBufferIterator<T> CircularBuffer<T>::begin()
    { return CircularBufferIterator<T>( *this, 0 ); }

    template <typename T>
    CircularBufferIterator<T> CircularBuffer<T>::end()
    { return CircularBufferIterator<T>( *this, size() ); }

    template <typename T>
    CircularBufferConstIterator<T> CircularBuffer<T>::begin() const
    { return cbegin(); }

    template <typename T>
    CircularBufferConstIterator<T> CircularBuffer<T>::end() const
    { return cend(); }

    template <typename T>
    CircularBufferConstIterator<T> CircularBuffer<T>::cbegin() const
    { return CircularBufferConstIterator<T>( *this, 0 ); }

    template <typename T>
    CircularBufferConstIterator<T> CircularBuffer<T>::cend() const
    { return CircularBufferConstIterator<T>( *this, size() ); }

    template <typename T>
    CircularBufferReverseIterator<T> CircularBuffer<T>::rbegin()
    { return CircularBufferReverseIterator<T>( *this, empty() ? 0u  : size() - 1 ); }

    template <typename T>
    CircularBufferReverseIterator<T> CircularBuffer<T>::rend()
    { return CircularBufferReverseIterator<T>( *this, size() ); }

    template <typename T>
    CircularBufferConstReverseIterator<T> CircularBuffer<T>::rbegin() const
    { return crbegin(); }

    template <typename T>
    CircularBufferConstReverseIterator<T> CircularBuffer<T>::rend() const
    { return crend(); }

    template <typename T>
    CircularBufferConstReverseIterator<T> CircularBuffer<T>::crbegin() const
    { return CircularBufferConstReverseIterator<T>( *this, empty() ? 0u  : size() - 1 ); }

    template <typename T>
    CircularBufferConstReverseIterator<T> CircularBuffer<T>::crend() const
    { return CircularBufferConstReverseIterator<T>( *this, size() ); }

    template <typename T>
    void CircularBuffer<T>::reserve( size_t new_capacity )
    {
        clear();
        free( mBuffer );
        mBuffer = reinterpret_cast<T*>( calloc( new_capacity, sizeof( T ) ) );
    }
}

#endif /* _CIRCULARBUFFER_H_ */
